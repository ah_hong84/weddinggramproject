//start an Angular app
//IIFE - Immediately Invoked Function Expression
(function(){

    angular.module("UploadApp", ["ngFileUpload"]); //"ngFileUpload" is a service built-in in the package, hence we don't need to create our own service

})();