//start an Angular app
//IIFE - Immediately Invoked Function Expression
(function(){

    angular
        .module("UploadApp")
        .controller("UploadCtrl", UploadCtrl);
    UploadCtrl.$inject = ["Upload"];

    function UploadCtrl(Upload){    
        var vm = this;
        vm.imgFile = null;  //initialize the object inside "Upload"
        vm.status = {
            message: "",
            code: 0
        };
    
    vm.upload = upload; //expose the function
    //vm.uploadS3 = uploadS3; //expose the function

        function upload(){
            Upload.upload({ //uploading the file to local disk
                //method: POST, //using POST method to upload the image file
                url: "/upload", //the url folder to post the image file
                data: {
                    "img-file": vm.imgFile
                }
            }).then(function(response){ //function to run if file is succesfully uploaded
                vm.fileurl = response.data;
                vm.status.message = "Successful";
                vm.status.code = 202;
            }).catch(function(err){ //function to run if file having error during upload
                console.log(err);
                vm.status.message = "Error";
                vm.status.code = 500;
            })

        }
    }

})();