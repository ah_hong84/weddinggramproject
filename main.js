/**** List of library Start ****
bower install
    - angular
    - bootstrap
    - font-awesome
    - jquery
    - ng-file-upload
    - ng-file-upload-shim

npm install
    - aws-sdk
    - body-parser
    - express
    - fs
    - multer
    - multer-s3
    - path
**** List of library End****/

//Load library
var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var fs = require("fs");
var multer = require("multer");
var AWS = require("aws-sdk");

//Create an instance of express app
var app = express();

//using multer to save the file into the folder in our local computer
var storage = multer.diskStorage({
    destination: "./upload_tmp",    //we set the saving destination folder in /upload_tmp
    filename: function(req, file, callback){    //set the name of the file we saved into /upload_tmp
        //console.log(file);
        callback(null, Date.now() + "-" + file.originalname);
    }
});

var upload = multer({
    storage: storage
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}));


//---> ADDED JAY
var allOrders = [];



var createRSVP = function(abc) {
    return ({
        name: abc.name,
        email: abc.email,
        tickets: abc.tickets,
        att: abc.att,
    });
}

// ADDED JAY END <-----


// config the port
app.set("port", parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000);

//For photo&comment --> parameters are going to be in a query string
app.get("/setPhList", function(req, res) {
    allPhoto.push(createPhList(req.query));
	res.status(201).end();
})

// module.exports = function(app){ //"app" is from server/app.js, where we use the libraries from there for this module
    app.post("/upload", upload.single("img-file"), function(req, res){ //it uses $http to upload the file with img file type
        console.log("Uploading....");
        fs.readFile(req.file.path, function(err, data){
            if(err){console.log("Error --> " + err);}
            res.status(202).json({
                size: req.file.size
            })
        });
    });
// }

//---> ADDED JAY

app.get("/rsvp", function(req, resp) {

    allOrders.push(createRSVP(req.query));
    //consle.log(allOrders)
    //console.log("-----")
    resp.status(201).end();

});


app.get("/rsvp-pending", function(req, resp) {
//  console.log(allOrders)
    resp.status(200);
    resp.type("application/json");
    resp.json(allOrders);
});

// ADDED JAY END <-----



//Define routes, load the static resources from the following directory
app.use("/libs", express.static(path.join(__dirname, "bower_components"))); //setting the path for /libs folder
app.use(express.static(path.join(__dirname, "/public")));
app.use(express.static(path.join(__dirname, "/gallery")));


// Start the server
app.listen(app.get("port"), function(){
    console.log("Application started at port --> %d" , app.get("port"));
    console.log("Application started on --> %s" , new Date());
});